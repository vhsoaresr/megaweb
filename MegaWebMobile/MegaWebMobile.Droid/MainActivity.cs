﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Prism.Unity;
using Microsoft.Practices.Unity;
using Acr.UserDialogs;
using Xamarin.Forms;
using FormsPlugin.Iconize.Droid;
using Plugin.Iconize;
using MegaWebMobile.Interfaces;
using Plugin.Media;
using Plugin.CurrentActivity;

[assembly: UsesFeature("android.hardware.camera", Required = false)]
[assembly: UsesFeature("android.hardware.camera.autofocus", Required = false)]
namespace MegaWebMobile.Droid
{
	[Activity(
		Label = "MegaWeb"
		, Icon = "@drawable/icon"
		, ConfigurationChanges = ConfigChanges.ScreenSize
		, ScreenOrientation = ScreenOrientation.Portrait
		, Theme = "@style/MyTheme.Splash"
		, MainLauncher = true
		, LaunchMode = LaunchMode.SingleTop
		)]
	public class MainActivity : Xamarin.Forms.Platform.Android.FormsAppCompatActivity
	{
		protected override void OnCreate(Bundle bundle)
		{
			SetTheme(Resource.Style.MyTheme);

			TabLayoutResource = Resource.Layout.tabs;
			ToolbarResource = Resource.Layout.toolbar;

			base.OnCreate(bundle);

			UserDialogs.Init(() => this);

			CrossCurrentActivity.Current.Init(this, bundle);

			Forms.Init(this, bundle);

			Iconize.With(new Plugin.Iconize.Fonts.FontAwesomeModule())
					.With(new Plugin.Iconize.Fonts.IoniconsModule());

			IconControls.Init(Resource.Id.toolbar, Resource.Id.sliding_tabs);

			LoadApplication(new App(new AndroidInitializer()));
		}

		public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
		{
			Plugin.Permissions.PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
		}
	}

	public class AndroidInitializer : IPlatformInitializer
	{
		public void RegisterTypes(IUnityContainer container)
		{
			container.RegisterInstance<IDataService>(new MegaWebMobile.Services.DataService());
		}
	}

}


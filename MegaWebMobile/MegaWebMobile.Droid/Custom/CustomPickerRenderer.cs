﻿using Android.Content;
using Android.Graphics.Drawables;
using MegaWebMobile.Custom;
using MegaWebMobile.Droid.Custom;
using MegaWebMobile.Infrastructure;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomPicker), typeof(CustomPickerRenderer))]
namespace MegaWebMobile.Droid.Custom
{
	public class CustomPickerRenderer : PickerRenderer
	{
		public CustomPickerRenderer(Context context) : base(context) { }

		protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
		{
			base.OnElementChanged(e);

			if (Control != null && e?.NewElement != null)
			{
				var background = new GradientDrawable();

				background.SetShape(ShapeType.Rectangle);
				background.SetColor(Android.Graphics.Color.Transparent);

				if (((CustomPicker)Element).BorderEnabled)
				{
					var corPrimaria = (Color)Application.Current.Resources["CorPrimaria"];
					background.SetStroke(2, Android.Graphics.Color.ParseColor(corPrimaria.GetHexString()));
					background.SetCornerRadius(0);
					var corSecundaria = (Color)Application.Current.Resources["CorSecundaria"];
					Control.SetTextColor(Android.Graphics.Color.ParseColor(corSecundaria.GetHexString()));
					Control.Typeface = Android.Graphics.Typeface.CreateFromAsset(Context.Assets, "Gotham-Book.ttf");
					Control.TextAlignment = Android.Views.TextAlignment.Center;
					Control.Gravity = Android.Views.GravityFlags.Center;
				}

				Control.SetBackground(background);

				//	Control.SetPadding(DpToPixels(Convert.ToSingle(12)), Control.PaddingTop, DpToPixels(Convert.ToSingle(12)), Control.PaddingBottom);
			}
		}
	}
}
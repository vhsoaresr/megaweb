﻿using Android.Content;
using MegaWebMobile.Custom;
using MegaWebMobile.Droid.Custom;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;


[assembly: ExportRenderer(typeof(CustomButton), typeof(CustomButtonRenderer))]
namespace MegaWebMobile.Droid.Custom
{
	public class CustomButtonRenderer : ButtonRenderer
	{
		public CustomButtonRenderer(Context context) : base(context) { }

		protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
		{
			base.OnElementChanged(e);

			if (Control != null)
			{
				Control.Elevation = 0;
				Control.SetPadding(0, 0, 0, 0);
			}

		}
	}
}
﻿using System;
using MegaWebMobile.Interfaces;
using System.IO;
using Xamarin.Forms;
using MegaWebMobile.Droid.Services;

[assembly: Dependency(typeof(FileHelper))]
namespace MegaWebMobile.Droid.Services
{
	public class FileHelper : IFileHelper
	{
		public string GetLocalFilePath(string filename)
		{
			string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			return Path.Combine(path, filename);
		}
	}
}
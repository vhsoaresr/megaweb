﻿using MegaWebMobile.Interfaces;
using Xamarin.Forms;
using MegaWebMobile.Droid.Services;
using Android.Content;

[assembly: Dependency(typeof(ClipboardService))]
namespace MegaWebMobile.Droid.Services
{
	public class ClipboardService : IClipboardService
	{
		public void CopyToClipboard(string text)
		{
			// Get the Clipboard Manager
#pragma warning disable 618
			var clipboardManager = (ClipboardManager)Forms.Context.GetSystemService(Context.ClipboardService);
#pragma warning restore 618

			// Create a new Clip
			ClipData clip = ClipData.NewPlainText("Copiar", text);

			// Copy the text
			clipboardManager.PrimaryClip = clip;
		}
	}
}
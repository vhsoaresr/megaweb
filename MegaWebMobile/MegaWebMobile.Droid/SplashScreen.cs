﻿using Android.App;
using Android.Content;
using Android.Support.V7.App;
using Android.Content.PM;

namespace MegaWebMobile.Droid
{
	[Activity(
	Label = "MegaWeb"
	, Theme = "@style/MyTheme.Splash"
	, MainLauncher = false
	, NoHistory = true
	, ScreenOrientation = ScreenOrientation.Portrait
	)]
	public class SplashActivity : AppCompatActivity
	{
		protected override void OnResume()
		{
			base.OnResume();

			SimulateStartup();
		}

		// Simulates background work that happens behind the splash screen
		void SimulateStartup()
		{
			StartActivity(new Intent(Application.Context, typeof(MainActivity)));
		}
	}
}
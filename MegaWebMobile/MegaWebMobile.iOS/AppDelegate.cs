﻿using Foundation;
using UIKit;
using Prism.Unity;
using Microsoft.Practices.Unity;
using FormsPlugin.Iconize.iOS;
using Plugin.Iconize;
using Xamarin.Forms.Platform.iOS;
using MegaWebMobile.Interfaces;
using System;
using MegaWebMobile.Services;
using UserNotifications;

namespace MegaWebMobile.iOS
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the 
	// User Interface of the application, as well as listening (and optionally responding) to 
	// application events from iOS.
	[Register("AppDelegate")]
	public partial class AppDelegate : FormsApplicationDelegate
	{
		//
		// This method is invoked when the application has loaded and is ready to run. In this 
		// method you should instantiate the window, load the UI into it and then make the window
		// visible.
		//
		// You have 17 seconds to return from this method, or iOS will terminate your application.
		//
		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
			Xamarin.Forms.Forms.Init();

			Iconize.With(new Plugin.Iconize.Fonts.FontAwesomeModule())
					.With(new Plugin.Iconize.Fonts.IoniconsModule());
			IconControls.Init();

			// Configurações do CrossLocalNotification Push
			if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
			{
				var authOptions = UNAuthorizationOptions.Alert | UNAuthorizationOptions.Badge | UNAuthorizationOptions.Sound;
				UNUserNotificationCenter.Current.RequestAuthorization(authOptions, (approved, error) => { });
			}
			else if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
			{
				var userOptions = UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound;
				var settings = UIUserNotificationSettings.GetSettingsForTypes(userOptions, new NSSet());
				UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
			}

			LoadApplication(new App(new iOSInitializer()));


			return base.FinishedLaunching(app, options);
		}
	}

	// ReSharper disable once InconsistentNaming
	public class iOSInitializer : IPlatformInitializer
	{
		public void RegisterTypes(IUnityContainer container)
		{
			UINavigationBar.Appearance.SetTitleTextAttributes(new UITextAttributes { TextColor = UIColor.White });

			container.RegisterInstance<IDataService>(new DataService());
		}
	}

}

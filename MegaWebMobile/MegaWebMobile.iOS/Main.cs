﻿using System;
using MegaWebMobile.Services;
using UIKit;

namespace MegaWebMobile.iOS
{
	public class Application
	{
		// This is the main entry point of the application.
		static void Main(string[] args)
		{
			UINavigationBar.Appearance.BarTintColor = UIColor.FromRGB(165, 60, 48);
			UINavigationBar.Appearance.TintColor = UIColor.White;

			UIApplication.Main(args, null, "AppDelegate");
		}
	}
}
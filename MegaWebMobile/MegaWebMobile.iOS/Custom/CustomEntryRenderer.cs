﻿using CoreGraphics;
using MegaWebMobile.Custom;
using MegaWebMobile.iOS.Custom;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace MegaWebMobile.iOS.Custom
{
	public class CustomEntryRenderer : EntryRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged(e);

			if (Control != null && e?.NewElement != null)
			{
				Control.BackgroundColor = UIColor.Clear;
				Control.BorderStyle = UITextBorderStyle.None;


				if (((CustomEntry)Element).BorderEnabled)
				{
					Control.LeftView = new UIView(new CGRect(0f, 0f, 9f, 20f));
					Control.LeftViewMode = UITextFieldViewMode.Always;
                    Control.Layer.BorderColor = UIColor.FromRGB(165,60,48).CGColor;
					Control.Layer.BorderWidth = 2;
					Control.ClipsToBounds = true;
                    Control.TextColor = UIColor.FromRGB(77,77,77);
					Control.Layer.CornerRadius = 0;
				}

			}

		}
	}
}
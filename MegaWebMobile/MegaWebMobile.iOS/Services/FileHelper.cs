﻿using System;
using MegaWebMobile.Interfaces;
using System.IO;
using Xamarin.Forms;
using MegaWebMobile.iOS.Services;

[assembly: Dependency(typeof(FileHelper))]
namespace MegaWebMobile.iOS.Services
{
	public class FileHelper : IFileHelper
	{
		public string GetLocalFilePath(string filename)
		{
			string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			return Path.Combine(path, filename);
		}
	}
}
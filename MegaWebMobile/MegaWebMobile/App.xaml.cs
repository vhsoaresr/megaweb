﻿using MegaWebMobile.ViewModels;
using MegaWebMobile.Views;
using Prism.Unity;
using Xamarin.Forms;
using System.Globalization;

namespace MegaWebMobile
{
	public partial class App : PrismApplication
	{
		public App() : this(null) { }
		public App(IPlatformInitializer initializer = null) : base(initializer) { }

		protected override void OnInitialized()
		{
			InitializeComponent();

			var ptBR = new CultureInfo("pt-BR");
			CultureInfo.DefaultThreadCurrentCulture = ptBR;
			CultureInfo.DefaultThreadCurrentUICulture = ptBR;

			NavigationService.NavigateAsync("/MainPage/NavigationPage/LevantamentosPage");
		}

		protected override void RegisterTypes()
		{
			Container.RegisterTypeForNavigation<NavigationPage>();
			Container.RegisterTypeForNavigation<MainPage, MainPageViewModel>();
			Container.RegisterTypeForNavigation<LevantamentosPage, LevantamentosPageViewModel>();
			Container.RegisterTypeForNavigation<LevantamentoEditPage, LevantamentoEditPageViewModel>();
		}
	}
}

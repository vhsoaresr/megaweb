using System.Collections.Generic;
using Prism.Navigation;
using MegaWebMobile.Infrastructure;
using System.Collections.ObjectModel;
using MegaWebMobile.Models;
using MegaWebMobile.Services;
using System.Windows.Input;
using Xamarin.Forms;
using MegaWebMobile.Views;
using System.Linq;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System.IO;
using System.Threading.Tasks;
using RestSharp.Portable;

namespace MegaWebMobile.ViewModels
{
	public class LevantamentoEditPageViewModel : ViewModelBase
	{
		private const string NOVOLEVANTAMENTO = "Novo Levantamento";
		private const string EDITARLEVANTAMENTO = "Editar Levantamento";

		private string _title;
		private Levantamento _levantamento;

		private List<Bloco> _blocos;
		private Bloco _selectedBloco;
		private List<Pavimento> _pavimentos;
		private Pavimento _selectedPavimento;
		private List<Luminaria> _luminarias;
		private Luminaria _selectedLuminaria;
		private List<Estado> _estados;
		private Estado _selectedEstado;

		public LevantamentoEditPageViewModel(INavigationService navigationService) : base(navigationService) { }

		public override void OnNavigatingTo(NavigationParameters parameters)
		{
			base.OnNavigatingTo(parameters);

			if (parameters.TryGetValue(nameof(Levantamento), out Levantamento levantamento))
			{
				Levantamento = levantamento;
				Title = EDITARLEVANTAMENTO;
				PrencheLists();
			}
			else
				Title = NOVOLEVANTAMENTO;
		}

		public string Title
		{
			get => _title;
			set => SetProperty(ref _title, value);
		}
		public Levantamento Levantamento
		{
			get => _levantamento ?? (_levantamento = new Levantamento());
			set => SetProperty(ref _levantamento, value);
		}

		public List<Bloco> Blocos
		{
			get => _blocos ?? (_blocos = new List<Bloco>().GetAll().ToList());
			set
			{
				SetProperty(ref _blocos, value);
				SelectedBloco = _blocos?.FirstOrDefault(o => o.Id == Levantamento.IdBloco);
			}
		}
		public Bloco SelectedBloco
		{
			get => _selectedBloco ?? (_selectedBloco = _blocos.FirstOrDefault(o => o.Id == Levantamento.IdBloco));
			set
			{
				SetProperty(ref _selectedBloco, value);
				Levantamento.IdBloco = value?.Id;
			}
		}
		public List<Pavimento> Pavimentos
		{
			get => _pavimentos ?? (_pavimentos = new List<Pavimento>().GetAll().ToList());
			set
			{
				SetProperty(ref _pavimentos, value);
				SelectedPavimento = _pavimentos?.FirstOrDefault(o => o.Id == Levantamento.IdPavimento);
			}
		}
		public Pavimento SelectedPavimento
		{
			get => _selectedPavimento ?? (_selectedPavimento = _pavimentos.FirstOrDefault(o => o.Id == Levantamento.IdPavimento));
			set
			{
				SetProperty(ref _selectedPavimento, value);
				Levantamento.IdPavimento = value?.Id;
			}
		}
		public List<Luminaria> Luminarias
		{
			get => _luminarias ?? (_luminarias = new List<Luminaria>().GetAll().ToList());
			set
			{
				SetProperty(ref _luminarias, value);
				SelectedLuminaria = _luminarias?.FirstOrDefault(o => o.Id == Levantamento.IdLuminaria);
			}
		}
		public Luminaria SelectedLuminaria
		{
			get => _selectedLuminaria ?? (_selectedLuminaria = _luminarias.FirstOrDefault(o => o.Id == Levantamento.IdLuminaria));
			set
			{
				SetProperty(ref _selectedLuminaria, value);
				Levantamento.IdLuminaria = value?.Id;
			}
		}
		public List<Estado> Estados
		{
			get => _estados ?? (_estados = new List<Estado>().GetAll().ToList());
			set
			{
				SetProperty(ref _estados, value);
				SelectedEstado = _estados?.FirstOrDefault(o => o.Id == Levantamento.IdEstado);
			}
		}
		public Estado SelectedEstado
		{
			get => _selectedEstado ?? (_selectedEstado = _estados.FirstOrDefault(o => o.Id == Levantamento.IdEstado));
			set
			{
				SetProperty(ref _selectedEstado, value);
				Levantamento.IdEstado = value?.Id;
			}
		}

		public ICommand SalvaCommand => new Command(SalvaLevantamento);
		public ICommand TiraFotoCommand => new Command(TiraFoto);
		public ICommand ExcluiFotoCommand => new Command(ExcluiFoto);

		private async void SalvaLevantamento()
		{
			try
			{
				Loading?.Show();

				Levantamento.Save();

				IRestResponse response;

				if (Title == NOVOLEVANTAMENTO)
					response = await Api.Levantamentos.Post(Levantamento);
				else
					response = await Api.Levantamentos.Put(Levantamento.Id, Levantamento);

				if (response.IsSuccess)
				{
					await DisplayAlert("Levantamento Salvo", "Levantamento Salvo com sucesso!");
					await NavigationService.GoBackAsync();
				}
				else
					await DisplayError(response.StatusDescription);
			}
			catch
			{
				await DisplayError();
			}
			finally
			{
				Loading?.Hide();
			}
		}
		private async void TiraFoto()
		{
			await CrossMedia.Current.Initialize();

			if (!CrossMedia.Current.IsTakePhotoSupported || !CrossMedia.Current.IsCameraAvailable)
			{
				await DisplayError("Nenhuma c�mera detectada.");
				return;
			}

			var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions());

			if (file == null)
				return;

			Levantamento.ImagemSource = ImageSource.FromFile(file.Path);

			using (var ms = new MemoryStream())
			{
				var stream = file.GetStream();
				await stream.CopyToAsync(ms);
				Levantamento.Imagem = ms.ToArray();
				file.Dispose();
			}

			Levantamento.Save();

			await EnviaFotoLevantamento(Levantamento);
		}
		private async Task EnviaFotoLevantamento(Levantamento levantamento)
		{
			try
			{
				Loading?.Show();

				var response = await Api.Levantamentos.Put(levantamento.Id, levantamento);

				if (response.IsSuccess)
				{
					await DisplayAlert("Foto Enviada", "Foto Enviada com sucesso!");
				}
				else
					await DisplayError(response.StatusDescription);
			}
			catch
			{
				await DisplayError();
			}
			finally
			{
				Loading?.Hide();
			}
		}
		private void ExcluiFoto()
		{
			Levantamento.ImagemSource = null;
			Levantamento.Imagem = null;
		}

		private void PrencheLists()
		{
			SelectedBloco = Blocos?.FirstOrDefault(o => o.Id == Levantamento?.IdBloco);
			SelectedPavimento = Pavimentos?.FirstOrDefault(o => o.Id == Levantamento?.IdPavimento);
			SelectedLuminaria = Luminarias?.FirstOrDefault(o => o.Id == Levantamento?.IdLuminaria);
			SelectedEstado = Estados?.FirstOrDefault(o => o.Id == Levantamento?.IdEstado);
		}
	}
}

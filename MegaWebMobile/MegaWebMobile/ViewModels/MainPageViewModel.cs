using Prism.Navigation;
using MegaWebMobile.Infrastructure;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using MegaWebMobile.Views;

namespace MegaWebMobile.ViewModels
{
	public class MainPageViewModel : ViewModelBase
	{
		private ObservableCollection<MenuItem> _menuOptions;
		private ObservableCollection<MenuItem> _menuOptions2;

		public MainPageViewModel(INavigationService navigationService) : base(navigationService)
		{
			MenuOptions = new ObservableCollection<MenuItem>
			{
				new MenuItem
				{
					Text = "Levantamentos",
					Command = NavigateCommand<LevantamentosPage>("NavigationPage/"),
					Icon = "fa-document"
				},
			};

			MenuOptions2 = new ObservableCollection<MenuItem>
			{
			};
		}

		public ObservableCollection<MenuItem> MenuOptions
		{
			get { return _menuOptions; }
			set { SetProperty(ref _menuOptions, value); }
		}
		public ObservableCollection<MenuItem> MenuOptions2
		{
			get { return _menuOptions2; }
			set { SetProperty(ref _menuOptions2, value); }
		}
	}
}

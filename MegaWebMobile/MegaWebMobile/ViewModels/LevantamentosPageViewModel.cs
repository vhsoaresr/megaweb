using System.Collections.Generic;
using Prism.Navigation;
using MegaWebMobile.Infrastructure;
using System.Collections.ObjectModel;
using MegaWebMobile.Models;
using MegaWebMobile.Services;
using System.Windows.Input;
using Xamarin.Forms;
using MegaWebMobile.Views;
using System.Linq;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System.IO;
using System.Threading.Tasks;

namespace MegaWebMobile.ViewModels
{
	public class LevantamentosPageViewModel : ViewModelBase
	{
		private ObservableCollection<Levantamento> _levantamentos;

		public LevantamentosPageViewModel(INavigationService navigationService) : base(navigationService) { }

		public override void OnNavigatedTo(NavigationParameters parameters)
		{
			base.OnNavigatedTo(parameters);
			if (parameters.GetValue<NavigationMode>("__NavigationMode") == NavigationMode.New)
				CarregaLevantamentos(true);
			else
				Levantamentos = new ObservableCollection<Levantamento>(new List<Levantamento>().GetAll().Where(o => !o.IsConcluido));
		}

		public ObservableCollection<Levantamento> Levantamentos
		{
			get => _levantamentos ?? (_levantamentos = new ObservableCollection<Levantamento>(new List<Levantamento>().GetAll().Where(o => !o.IsConcluido)));
			set => SetProperty(ref _levantamentos, value);
		}

		public ICommand RefreshCommand => new Command(() => CarregaLevantamentos());
		public ICommand CreateCommand => NavigateCommand<LevantamentoEditPage>();
		public ICommand EditCommand => new Command<int>(EditLevantamento);
		public ICommand ConcluiCommand => new Command<int>(ConcluiLevantamento);
		public ICommand TiraFotoCommand => new Command<int>(TiraFoto);

		private async void CarregaLevantamentos(bool carregaTabelas = false)
		{
			try
			{
				Loading?.Show();

				if (carregaTabelas)
					if (!await SincronizaTabelas())
						return;

				var response = await Api.Levantamentos.Get();

				if (response.IsSuccess)
					Levantamentos = new ObservableCollection<Levantamento>(await response.Data.SaveAllAsync());
				else
					await DisplayError(response.StatusDescription);
			}
			catch
			{
				await DisplayError();
			}
			finally
			{
				Loading?.Hide();
				IsRefreshing = false;
			}
		}
		private void EditLevantamento(int id)
		{
			var parms = new NavigationParameters
			{
				{ nameof(Levantamento), Levantamentos.FirstOrDefault(o => o.Id == id) }
			};
			Navigate<LevantamentoEditPage>(parms);
		}
		private async void ConcluiLevantamento(int id)
		{
			try
			{
				var levantamento = Levantamentos.First(o => o.Id == id);

				if (levantamento.Imagem == null)
				{
					await DisplayAlert("Ops!", "Para concluir voc� precisa tirar uma foto.");
					return;
				}

				Loading?.Show();

				levantamento.IsConcluido = true;

				var response = await Api.Levantamentos.Put(id, levantamento);

				if (response.IsSuccess)
				{
					Levantamentos.Remove(levantamento);
					await DisplayAlert("Levantamento Conclu�do", "Levantamento conclu�do com sucesso!");
				}
				else
					await DisplayError(response.StatusDescription);
			}
			catch
			{
				await DisplayError();
			}
			finally
			{
				Loading?.Hide();
			}
		}
		private async void TiraFoto(int id)
		{
			await CrossMedia.Current.Initialize();

			if (!CrossMedia.Current.IsTakePhotoSupported || !CrossMedia.Current.IsCameraAvailable)
			{
				await DisplayError("Nenhuma c�mera detectada.");
				return;
			}

			var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
			{
				PhotoSize = PhotoSize.Medium
			});

			if (file == null)
				return;

			var levantamento = Levantamentos.First(o => o.Id == id);

			levantamento.ImagemSource = ImageSource.FromFile(file.Path);

			using (var ms = new MemoryStream())
			{
				var stream = file.GetStream();
				await stream.CopyToAsync(ms);
				levantamento.Imagem = ms.ToArray();
				file.Dispose();
			}

			levantamento.Save();
			Levantamentos = new ObservableCollection<Levantamento>(new List<Levantamento>().GetAll().Where(o => !o.IsConcluido));

			await EnviaFotoLevantamento(levantamento);
		}
		private async Task EnviaFotoLevantamento(Levantamento levantamento)
		{
			try
			{
				Loading?.Show();

				var response = await Api.Levantamentos.Put(levantamento.Id, levantamento);

				if (response.IsSuccess)
				{
					await DisplayAlert("Foto Enviada", "Foto Enviada com sucesso!");
				}
				else
					await DisplayError(response.StatusDescription);
			}
			catch
			{
				await DisplayError();
			}
			finally
			{
				Loading?.Hide();
			}
		}
	}
}
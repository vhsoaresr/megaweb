﻿using MegaWebMobile.Infrastructure;
using Newtonsoft.Json;
using SQLite;
using System.Collections.Generic;
using System.IO;
using Xamarin.Forms;

namespace MegaWebMobile.Models
{
	public class Levantamento : TableClass
	{
		private ImageSource _imagemSource;
		private Pavimento _pavimento;
		private Bloco _bloco;
		private Luminaria _luminaria;
		private Estado _estado;
		private bool _isVisible;
		private bool _isImageVisible;
		private string _iconExpansable = "fa-angle-double-down";

		[PrimaryKey]
		public int Id { get; set; }
		public string Descricao { get; set; }
		public int? IdPavimento { get; set; }
		public int? IdBloco { get; set; }
		public int? IdLuminaria { get; set; }
		public int? IdEstado { get; set; }
		public byte[] Imagem { get; set; }
		public bool IsConcluido { get; set; }

		[Ignore, JsonIgnore]
		public ImageSource ImagemSource
		{
			get
			{
				if (_imagemSource == null && Imagem != null)
				{
					ImagemSource = ImageSource.FromStream(() => new MemoryStream(Imagem));
					RaisePropertyChanged(nameof(ImagemSource));
				}

				return _imagemSource;
			}

			set => SetProperty(ref _imagemSource, value);
		}

		[Ignore, JsonIgnore]
		public Pavimento Pavimento
		{
			get => _pavimento ?? (_pavimento = new List<Pavimento>().GetAll().FirstOrDefault(o => o.Id == IdPavimento));
			set => SetProperty(ref _pavimento, value);
		}
		[Ignore, JsonIgnore]
		public Bloco Bloco
		{
			get => _bloco ?? (_bloco = new List<Bloco>().GetAll().FirstOrDefault(o => o.Id == IdBloco));
			set => SetProperty(ref _bloco, value);
		}
		[Ignore, JsonIgnore]
		public Luminaria Luminaria
		{
			get => _luminaria ?? (_luminaria = new List<Luminaria>().GetAll().FirstOrDefault(o => o.Id == IdLuminaria));
			set => SetProperty(ref _luminaria, value);
		}
		[Ignore, JsonIgnore]
		public Estado Estado
		{
			get => _estado ?? (_estado = new List<Estado>().GetAll().FirstOrDefault(o => o.Id == IdEstado));
			set => SetProperty(ref _estado, value);
		}

		[Ignore, JsonIgnore]
		public bool IsVisible
		{
			get => _isVisible;
			set
			{
				SetProperty(ref _isVisible, value);
				RaisePropertyChanged(nameof(IsImageVisible));
				RaisePropertyChanged(nameof(IsImageButtonVisible));
				RaisePropertyChanged(nameof(ImagemSource));
			}
		}
		[Ignore, JsonIgnore]
		public bool IsImageVisible
		{
			get => IsVisible && (_isImageVisible = ImagemSource != null);
			set => SetProperty(ref _isImageVisible, value);
		}
		[Ignore, JsonIgnore]
		public bool IsImageButtonVisible
		{
			get => IsVisible && !IsImageVisible;
			set => SetProperty(ref _isImageVisible, !value);
		}
		[Ignore, JsonIgnore]
		public string IconExpansable
		{
			get => _iconExpansable;
			set => SetProperty(ref _iconExpansable, value);
		}
	}
}

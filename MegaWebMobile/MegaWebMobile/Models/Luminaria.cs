﻿using MegaWebMobile.Infrastructure;

namespace MegaWebMobile.Models
{
	public class Luminaria : TableClass
	{
		[SQLite.PrimaryKey]
		public int Id { get; set; }
		public string Descricao { get; set; }
	}
}

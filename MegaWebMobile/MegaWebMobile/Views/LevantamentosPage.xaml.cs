using MegaWebMobile.Models;
using Xamarin.Forms;

namespace MegaWebMobile.Views
{
	public partial class LevantamentosPage : ContentPage
	{
		public LevantamentosPage()
		{
			InitializeComponent();
		}

		private void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
		{
			var simu = e.Item as Levantamento;

			if (simu.IsVisible)
			{
				simu.IsVisible = false;
				simu.IconExpansable = "fa-angle-double-down";
			}
			else
			{
				simu.IsVisible = true;
				simu.IconExpansable = "fa-angle-double-up";
			}
		}
	}
}
﻿using MegaWebMobile.Interfaces;
using System;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Net;
using Newtonsoft.Json;

namespace MegaWebMobile.Services
{
	public class RestService : IRestService, IDisposable
	{
		private readonly int _timeOut = (int)Application.Current.Resources["ApiTimeOut"];
		private readonly RestClient _client;

		public RestService(string url, bool useAuth = true)
		{
			_client = new RestClient
			{
				BaseUrl = new Uri(UrlApi),
				Timeout = TimeSpan.FromSeconds(_timeOut),
				IgnoreResponseStatusCode = true
			};

			Request = new RestRequest(url);

			if (useAuth)
				Request.AddParameter("Authorization", $"Bearer {Token}", ParameterType.HttpHeader);
		}

		public static string UrlApi
		{
			get
			{
#if DEBUG
				return (string)Application.Current.Resources["ApiTesteUri"];
#elif QA
				return (string)Application.Current.Resources["ApiTesteUri"];
#else
				return (string)Application.Current.Resources["ApiUri"];
#endif
			}
		}

		public static string Token
		{
			get => Application.Current.Properties.TryGetValue("Token", out var token) ? (string)token : null;
			set => Application.Current.Properties["Token"] = value;
		}
		public RestRequest Request { get; }

		public async Task<IRestResponse<T>> Get<T>()
		{
			Request.Method = Method.GET;

			Request.AddHeader("Accept", "application/json");

			var response = await _client.Execute<T>(Request);

			return response;
		}
		public async Task<IRestResponse> Post(object jsonBody = null)
		{
			Request.Method = Method.POST;

			Request.AddHeader("Accept", "application/json");
			Request.AddJsonBody(jsonBody);

			var response = await _client.Execute(Request);

			return response;
		}
		public async Task<IRestResponse> Put(object jsonBody = null)
		{
			Request.Method = Method.PUT;

			Request.AddHeader("Accept", "application/json");
			Request.AddJsonBody(jsonBody);

			var response = await _client.Execute(Request);

			return response;
		}
		public async Task<IRestResponse> Delete()
		{
			Request.Method = Method.DELETE;

			Request.AddHeader("Accept", "application/json");

			var response = await _client.Execute(Request);

			return response;
		}

		public void Dispose()
		{
			_client?.Dispose();
		}
	}
}
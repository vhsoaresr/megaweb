﻿using MegaWebMobile.Interfaces;
using MegaWebMobile.Models;
using SQLite;
using Xamarin.Forms;

namespace MegaWebMobile.Services
{
	public class DataService : IDataService
	{
		private static SQLiteConnection _connection;
		private static SQLiteAsyncConnection _connectionAsync;

		public DataService()
		{
			if (_connection != null) return;

			var dbFile = DependencyService.Get<IFileHelper>().GetLocalFilePath("data.db");

			_connectionAsync = new SQLiteAsyncConnection(dbFile);
			_connection = _connectionAsync.GetConnection();

			CriaTabelas();
		}

		public SQLiteConnection DB => _connection;
		public SQLiteAsyncConnection DBAsync => _connectionAsync;

		private static void CriaTabelas()
		{
			_connection.CreateTable<Levantamento>();
			_connection.CreateTable<Bloco>();
			_connection.CreateTable<Estado>();
			_connection.CreateTable<Pavimento>();
			_connection.CreateTable<Luminaria>();
		}
		public static void ApagaTabelas()
		{
			_connection.DeleteAll<Levantamento>();
			_connection.DeleteAll<Bloco>();
			_connection.DeleteAll<Estado>();
			_connection.DeleteAll<Pavimento>();
			_connection.DeleteAll<Luminaria>();
		}
	}
}

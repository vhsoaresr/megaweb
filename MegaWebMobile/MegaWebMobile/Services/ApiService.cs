﻿using RestSharp.Portable;
using System.Threading.Tasks;
using System;
using MegaWebMobile.Models;
using System.Collections.Generic;

namespace MegaWebMobile.Services
{
	public static class Api
	{
		public static class Levantamentos
		{
			public static async Task<IRestResponse<List<Levantamento>>> Get()
			{
				using (var client = new RestService("/api/Levantamentos", false))
				{
					return await client.Get<List<Levantamento>>();
				}
			}
			public static async Task<IRestResponse<Levantamento>> Get(int id)
			{
				using (var client = new RestService($"/api/Levantamentos/{id}", false))
				{
					return await client.Get<Levantamento>();
				}
			}
			public static async Task<IRestResponse> Put(int id, Levantamento levantamento)
			{
				using (var client = new RestService($"/api/Levantamentos/{id}", false))
				{
					return await client.Put(levantamento);
				}
			}
			public static async Task<IRestResponse> Post(Levantamento levantamento)
			{
				using (var client = new RestService("/api/Levantamentos", false))
				{
					return await client.Post(levantamento);
				}
			}
			public static async Task<IRestResponse> Delete(int id)
			{
				using (var client = new RestService($"/api/Levantamentos/{id}", false))
				{
					return await client.Delete();
				}
			}
		}
		public static class Blocos
		{
			public static async Task<IRestResponse<List<Bloco>>> Get()
			{
				using (var client = new RestService("/api/Blocos", false))
				{
					return await client.Get<List<Bloco>>();
				}
			}
			public static async Task<IRestResponse<Bloco>> Get(int id)
			{
				using (var client = new RestService($"/api/Blocos/{id}", false))
				{
					return await client.Get<Bloco>();
				}
			}
			public static async Task<IRestResponse> Put(int id, Bloco bloco)
			{
				using (var client = new RestService($"/api/Blocos/{id}", false))
				{
					return await client.Put(bloco);
				}
			}
			public static async Task<IRestResponse> Post(Bloco bloco)
			{
				using (var client = new RestService("/api/Blocos", false))
				{
					return await client.Post(bloco);
				}
			}
			public static async Task<IRestResponse> Delete(int id)
			{
				using (var client = new RestService($"/api/Blocos/{id}", false))
				{
					return await client.Delete();
				}
			}
		}
		public static class Pavimentos
		{
			public static async Task<IRestResponse<List<Pavimento>>> Get()
			{
				using (var client = new RestService("/api/Pavimentos", false))
				{
					return await client.Get<List<Pavimento>>();
				}
			}
			public static async Task<IRestResponse<Pavimento>> Get(int id)
			{
				using (var client = new RestService($"/api/Pavimentos/{id}", false))
				{
					return await client.Get<Pavimento>();
				}
			}
			public static async Task<IRestResponse> Put(int id, Pavimento pavimento)
			{
				using (var client = new RestService($"/api/Pavimentos/{id}", false))
				{
					return await client.Put(pavimento);
				}
			}
			public static async Task<IRestResponse> Post(Pavimento pavimento)
			{
				using (var client = new RestService("/api/Pavimentos", false))
				{
					return await client.Post(pavimento);
				}
			}
			public static async Task<IRestResponse> Delete(int id)
			{
				using (var client = new RestService($"/api/Pavimentos/{id}", false))
				{
					return await client.Delete();
				}
			}
		}
		public static class Luminarias
		{
			public static async Task<IRestResponse<List<Luminaria>>> Get()
			{
				using (var client = new RestService("/api/Luminarias", false))
				{
					return await client.Get<List<Luminaria>>();
				}
			}
			public static async Task<IRestResponse<Luminaria>> Get(int id)
			{
				using (var client = new RestService($"/api/Luminarias/{id}", false))
				{
					return await client.Get<Luminaria>();
				}
			}
			public static async Task<IRestResponse> Put(int id, Luminaria luminaria)
			{
				using (var client = new RestService($"/api/Luminarias/{id}", false))
				{
					return await client.Put(luminaria);
				}
			}
			public static async Task<IRestResponse> Post(Luminaria luminaria)
			{
				using (var client = new RestService("/api/Luminarias", false))
				{
					return await client.Post(luminaria);
				}
			}
			public static async Task<IRestResponse> Delete(int id)
			{
				using (var client = new RestService($"/api/Luminarias/{id}", false))
				{
					return await client.Delete();
				}
			}
		}
		public static class Estados
		{
			public static async Task<IRestResponse<List<Estado>>> Get()
			{
				using (var client = new RestService("/api/Estados", false))
				{
					return await client.Get<List<Estado>>();
				}
			}
			public static async Task<IRestResponse<Estado>> Get(int id)
			{
				using (var client = new RestService($"/api/Estados/{id}", false))
				{
					return await client.Get<Estado>();
				}
			}
			public static async Task<IRestResponse> Put(int id, Estado estado)
			{
				using (var client = new RestService($"/api/Estados/{id}", false))
				{
					return await client.Put(estado);
				}
			}
			public static async Task<IRestResponse> Post(Estado estado)
			{
				using (var client = new RestService("/api/Estados", false))
				{
					return await client.Post(estado);
				}
			}
			public static async Task<IRestResponse> Delete(int id)
			{
				using (var client = new RestService($"/api/Estados/{id}", false))
				{
					return await client.Delete();
				}
			}
		}
	}
}

﻿using Xamarin.Forms;

namespace MegaWebMobile.Custom
{
	public class CustomEntry : Entry
	{
		public CustomEntry()
		{
			BackgroundColor = Color.Transparent;
		}

		public static readonly BindableProperty BorderEnabledProperty = 
		BindableProperty.Create(
			nameof(BorderEnabled),
			typeof(bool), 
			typeof(CustomEntry), 
			true);
		public bool BorderEnabled
		{
			get => (bool)GetValue(BorderEnabledProperty);
			set => SetValue(BorderEnabledProperty, value);
		}
	}
}

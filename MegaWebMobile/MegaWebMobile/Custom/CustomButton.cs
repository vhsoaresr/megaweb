﻿using Xamarin.Forms;

namespace MegaWebMobile.Custom
{
	public class CustomButton : Button
	{
		public CustomButton()
		{
			BorderWidth = 1;
			BorderRadius = 7;
			FontSize = 16;
			FontFamily = (OnPlatform<string>)Application.Current.Resources["FontPadrao"];
			HorizontalOptions = LayoutOptions.CenterAndExpand;
		}
	}
}

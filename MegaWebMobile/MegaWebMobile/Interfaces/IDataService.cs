﻿using SQLite;

namespace MegaWebMobile.Interfaces
{
	public interface IDataService
	{
		SQLiteConnection DB { get; }
		SQLiteAsyncConnection DBAsync { get; }
	}
}

﻿namespace MegaWebMobile.Interfaces
{
	public interface IClipboardService
	{
		void CopyToClipboard(string text);
	}
}
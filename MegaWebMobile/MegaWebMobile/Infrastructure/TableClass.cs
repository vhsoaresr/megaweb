﻿using MegaWebMobile.Services;
using SQLite;
using System;
using System.Collections.Generic;
using Prism.Mvvm;
using System.Linq;
using System.Threading.Tasks;

namespace MegaWebMobile.Infrastructure
{
	public abstract class TableClass : BindableBase { }

	public static class TableExtensions
	{
		private static readonly SQLiteConnection Db = new DataService().DB;
		private static readonly SQLiteAsyncConnection DbAsync = new DataService().DBAsync;

		public static TableQuery<T> GetAll<T>(this IEnumerable<T> list) where T : new()
		{
			if (list == null) return null;

			var result = Db.Table<T>();
			list = result;
			return result;

		}
		public static T Get<T>(this T table, int id) where T : new()
		{
			if (table == null) return default(T);

			table = Db.Get<T>(id);
			return table;
		}
		public static T Save<T>(this T table) where T : TableClass
		{
			Db.InsertOrReplace(table);

			return table;
		}
		public static T Save<T>(this T table, T tableClass) where T : TableClass
		{
			if (table == null)
				return null;

			table = tableClass;
			return table.Save();
		}
		public static T Delete<T>(this T table) where T : TableClass
		{
			Db.Delete(table);

			return table;
		}
		public static async Task<IEnumerable<T>> SaveAllAsync<T>(this IEnumerable<T> list) where T : TableClass
		{
			var saveAll = list.ToList();
			foreach (var item in saveAll)
				await DbAsync.InsertOrReplaceAsync(item);

			return saveAll;
		}
		public static IEnumerable<T> DeleteAll<T>(this IEnumerable<T> list) where T : TableClass
		{
			Db.DeleteAll<T>();

			return list;
		}
	}
}
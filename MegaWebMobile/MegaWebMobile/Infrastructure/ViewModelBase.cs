using System;
using Prism.Mvvm;
using Prism.Navigation;
using Xamarin.Forms;
using System.Threading.Tasks;
using Acr.UserDialogs;
using MegaWebMobile.Services;
using System.Windows.Input;

namespace MegaWebMobile.Infrastructure
{
	public abstract class ViewModelBase : BindableBase, INavigationAware, IConfirmNavigation
	{
		private bool _isRefreshing;

		protected ViewModelBase(INavigationService navigationService)
		{
			NavigationService = navigationService ?? throw new ArgumentNullException(nameof(navigationService));
		}

		protected INavigationService NavigationService { get; }

		protected IUserDialogs UserDialogs => Acr.UserDialogs.UserDialogs.Instance;
		protected Loading Loading => new Loading(UserDialogs, IsRefreshing);

		public bool IsRefreshing
		{
			get => _isRefreshing;
			set => SetProperty(ref _isRefreshing, value);
		}

		private async void Navigate(string uri, string prefix = "", NavigationParameters parameters = null, bool useModal = false)
		{
			if (string.IsNullOrEmpty(uri))
				return;

			await NavigationService.NavigateAsync(prefix + uri, parameters, useModal);
		}

		protected void Navigate<T>(NavigationParameters parameters = null, bool useModal = false) where T : Page
		{
			Navigate(typeof(T).Name, "", parameters, useModal);
		}
		protected void Navigate<T>(string prefix, NavigationParameters parameters = null) where T : Page
		{
			Navigate(typeof(T).Name, prefix, parameters);
		}
		protected void NavigateMain<T>(NavigationParameters parameters = null) where T : Page
		{
			Navigate(typeof(T).Name, "/MainPage/NavigationPage/", parameters);
		}
		protected ICommand NavigateCommand<T>(NavigationParameters parameters = null) where T : Page
		{
			return new Command(() => { Navigate<T>(parameters); });
		}
		protected ICommand NavigateCommand<T>(string prefix, NavigationParameters parameters = null) where T : Page
		{
			return new Command(() => { Navigate<T>(prefix, parameters); });
		}

		public virtual void OnNavigatedFrom(NavigationParameters parameters) { }
		public virtual void OnNavigatedTo(NavigationParameters parameters) { }
		public virtual void OnNavigatingTo(NavigationParameters parameters) { }
		public virtual bool CanNavigate(NavigationParameters parameters) => true;

		protected Task DisplayAlert(string title, string message)
		{
			var retorno = new Task(() => { });

			Loading?.Hide();

			if (string.IsNullOrWhiteSpace(title) || string.IsNullOrWhiteSpace(message))
				return retorno;

			retorno = UserDialogs.AlertAsync(message, title, "OK");

			return retorno;
		}
		protected Task DisplayError(string message = "N�o foi possivel atualizar os dados, verifique sua conex�o", string title = "Sincroniza��o de Dados")
		{
			if (string.IsNullOrWhiteSpace(message))
				message = "N�o foi possivel atualizar os dados, verifique sua conex�o";

			return DisplayAlert(title, message);
		}

		protected async Task<bool> SincronizaTabelas()
		{
			try
			{
				await Task.WhenAll(
					BaixaBlocos(),
					BaixaPavimentos(),
					BaixaLuminarias(),
					BaixaEstados()
				);
				return true;
			}
			catch (TaskCanceledException)
			{
				await DisplayError();
			}
			catch (Exception ex)
			{
				await DisplayError(ex.Message);
			}

			return false;
		}
		protected async Task BaixaBlocos()
		{
			var response = await Api.Blocos.Get();

			if (response.IsSuccess)
				await response.Data.SaveAllAsync();
			else
				throw new Exception(response.StatusDescription);
		}
		protected async Task BaixaPavimentos()
		{
			var response = await Api.Pavimentos.Get();

			if (response.IsSuccess)
				await response.Data.SaveAllAsync();
			else
				throw new Exception(response.StatusDescription);
		}
		protected async Task BaixaLuminarias()
		{
			var response = await Api.Luminarias.Get();

			if (response.IsSuccess)
				await response.Data.SaveAllAsync();
			else
				throw new Exception(response.StatusDescription);
		}
		protected async Task BaixaEstados()
		{
			var response = await Api.Estados.Get();

			if (response.IsSuccess)
				await response.Data.SaveAllAsync();
			else
				throw new Exception(response.StatusDescription);
		}

	}
}
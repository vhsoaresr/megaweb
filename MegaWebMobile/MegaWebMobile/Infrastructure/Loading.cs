﻿using Acr.UserDialogs;

namespace MegaWebMobile.Infrastructure
{
	public class Loading
	{
		private readonly IUserDialogs _userDialogs;
		private readonly bool _isRefreshing;

		public Loading(IUserDialogs userDialogs, bool isRefreshing)
		{
			_userDialogs = userDialogs;
			_isRefreshing = isRefreshing;
		}

		private IProgressDialog ProgressDialog => _userDialogs?.Loading("");

		public void Show()
		{
			try
			{
				if (!_isRefreshing)
					ProgressDialog.Show();
			}
			catch { }
		}
		public void Hide()
		{
			try
			{
				if (!_isRefreshing)
					ProgressDialog.Hide();
			}
			catch { }
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MegaWeb.Models
{
    public partial class Estado
    {
        public Estado()
        {
            Levantamento = new HashSet<Levantamento>();
        }

        public int Id { get; set; }
		[Display(Name = "Descrição")]
		public string Descricao { get; set; }

        public ICollection<Levantamento> Levantamento { get; set; }
    }
}

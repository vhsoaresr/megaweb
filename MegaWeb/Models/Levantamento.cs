﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MegaWeb.Models
{
    public partial class Levantamento
    {
        public int Id { get; set; }
		[Display(Name = "Descrição")]
		public string Descricao { get; set; }
		[Display(Name = "Pavimento")]
		public int? IdPavimento { get; set; }
		[Display(Name = "Bloco")]
		public int? IdBloco { get; set; }
		[Display(Name = "Luminária")]
		public int? IdLuminaria { get; set; }
		[Display(Name = "Estado")]
		public int? IdEstado { get; set; }
        public byte[] Imagem { get; set; }
		[Display(Name = "Concluído?")]
		public bool IsConcluido { get; set; }

		[Display(Name = "Bloco")]
        public Bloco IdBlocoNavigation { get; set; }
		[Display(Name = "Estado")]
		public Estado IdEstadoNavigation { get; set; }
		[Display(Name = "Luminária")]
		public Luminaria IdLuminariaNavigation { get; set; }
		[Display(Name = "Pavimento")]
		public Pavimento IdPavimentoNavigation { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MegaWeb.Data;
using MegaWeb.Models;

namespace MegaWeb.Controllers
{
	public class EstadosController : Controller
	{
		private readonly FieldDBContext _context;

		public EstadosController(FieldDBContext context)
		{
			_context = context;
		}

		#region MVC
		[Route("Estados")]
		public async Task<IActionResult> Index()
		{
			return View(await _context.Estado.ToListAsync());
		}

		[Route("Estados/Create")]
		public IActionResult Create()
		{
			return View();
		}

		[HttpPost]
		[Route("Estados/Create")]
		public async Task<IActionResult> Create([Bind("Id,Descricao")] Estado estados)
		{
			if (ModelState.IsValid)
			{
				_context.Add(estados);
				await _context.SaveChangesAsync();
				return RedirectToAction(nameof(Index));
			}
			return View(estados);
		}

		[Route("Estados/Edit/{id}")]
		public async Task<IActionResult> Edit(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var estados = await _context.Estado.SingleOrDefaultAsync(m => m.Id == id);
			if (estados == null)
			{
				return NotFound();
			}
			return View(estados);
		}

		[HttpPost]
		[Route("Estados/Edit/{id}")]
		public async Task<IActionResult> Edit(int id, [Bind("Id,Descricao")] Estado estados)
		{
			if (id != estados.Id)
			{
				return NotFound();
			}

			if (ModelState.IsValid)
			{
				try
				{
					_context.Update(estados);
					await _context.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!EstadoExists(estados.Id))
					{
						return NotFound();
					}
					else
					{
						throw;
					}
				}
				return RedirectToAction(nameof(Index));
			}
			return View(estados);
		}

		[Route("Estados/Delete/{id}")]
		public async Task<IActionResult> Delete(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var estados = await _context.Estado
				.SingleOrDefaultAsync(m => m.Id == id);
			if (estados == null)
			{
				return NotFound();
			}

			return View(estados);
		}

		[HttpPost, ActionName("Delete")]
		[Route("Estados/Delete/{id}")]
		public async Task<IActionResult> DeleteConfirmed(int id)
		{
			var estados = await _context.Estado.SingleOrDefaultAsync(m => m.Id == id);
			_context.Estado.Remove(estados);
			await _context.SaveChangesAsync();
			return RedirectToAction(nameof(Index));
		}
		#endregion

		#region API
		[HttpGet]
		[Route("api/Estados")]
		public IEnumerable<Estado> Get()
		{
			return _context.Estado;
		}

		[HttpGet]
		[Route("api/Estados/{id}")]
		public async Task<IActionResult> Get([FromRoute] int id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var estado = await _context.Estado.SingleOrDefaultAsync(m => m.Id == id);

			if (estado == null)
			{
				return NotFound();
			}

			return Ok(estado);
		}

		[HttpPut("{id}")]
		[Route("api/Estados/{id}")]
		public async Task<IActionResult> Puto([FromRoute] int id, [FromBody] Estado estado)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (id != estado.Id)
			{
				return BadRequest();
			}

			_context.Entry(estado).State = EntityState.Modified;

			try
			{
				await _context.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!EstadoExists(id))
				{
					return NotFound();
				}
				else
				{
					throw;
				}
			}

			return NoContent();
		}

		[HttpPost]
		[Route("api/Estados")]
		public async Task<IActionResult> Post([FromBody] Estado estado)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			_context.Estado.Add(estado);
			await _context.SaveChangesAsync();

			return CreatedAtAction("Get", new { id = estado.Id }, estado);
		}

		[HttpDelete]
		[Route("api/Estados/{id}")]
		public async Task<IActionResult> Delete([FromRoute] int id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var estado = await _context.Estado.SingleOrDefaultAsync(m => m.Id == id);
			if (estado == null)
			{
				return NotFound();
			}

			_context.Estado.Remove(estado);
			await _context.SaveChangesAsync();

			return Ok(estado);
		}
		#endregion

		private bool EstadoExists(int id)
		{
			return _context.Estado.Any(e => e.Id == id);
		}
	}
}

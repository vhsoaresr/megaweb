﻿using Microsoft.AspNetCore.Mvc;

namespace MegaWeb.Controllers
{
	public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MegaWeb.Data;
using MegaWeb.Models;

namespace MegaWeb.Controllers
{
	public class PavimentosController : Controller
	{
		private readonly FieldDBContext _context;

		public PavimentosController(FieldDBContext context)
		{
			_context = context;
		}

		#region MVC
		[Route("Pavimentos")]
		public async Task<IActionResult> Index()
		{
			return View(await _context.Pavimento.ToListAsync());
		}

		[Route("Pavimentos/Create")]
		public IActionResult Create()
		{
			return View();
		}

		[HttpPost]
		[Route("Pavimentos/Create")]
		public async Task<IActionResult> Create([Bind("Id,Descricao")] Pavimento pavimento)
		{
			if (ModelState.IsValid)
			{
				_context.Add(pavimento);
				await _context.SaveChangesAsync();
				return RedirectToAction(nameof(Index));
			}
			return View(pavimento);
		}

		[Route("Pavimentos/Edit/{id}")]
		public async Task<IActionResult> Edit(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var pavimentos = await _context.Pavimento.SingleOrDefaultAsync(m => m.Id == id);
			if (pavimentos == null)
			{
				return NotFound();
			}
			return View(pavimentos);
		}

		[HttpPost]
		[Route("Pavimentos/Edit/{id}")]
		public async Task<IActionResult> Edit(int id, [Bind("Id,Descricao")] Pavimento pavimento)
		{
			if (id != pavimento.Id)
			{
				return NotFound();
			}

			if (ModelState.IsValid)
			{
				try
				{
					_context.Update(pavimento);
					await _context.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!PavimentoExists(pavimento.Id))
					{
						return NotFound();
					}
					else
					{
						throw;
					}
				}
				return RedirectToAction(nameof(Index));
			}
			return View(pavimento);
		}

		[Route("Pavimentos/Delete/{id}")]
		public async Task<IActionResult> Delete(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var pavimentos = await _context.Pavimento
				.SingleOrDefaultAsync(m => m.Id == id);
			if (pavimentos == null)
			{
				return NotFound();
			}

			return View(pavimentos);
		}

		[HttpPost, ActionName("Delete")]
		[Route("Pavimentos/Delete/{id}")]
		public async Task<IActionResult> DeleteConfirmed(int id)
		{
			var pavimentos = await _context.Pavimento.SingleOrDefaultAsync(m => m.Id == id);
			_context.Pavimento.Remove(pavimentos);
			await _context.SaveChangesAsync();
			return RedirectToAction(nameof(Index));
		}
		#endregion

		#region API
		[HttpGet]
		[Route("api/Pavimentos")]
		public IEnumerable<Pavimento> Get()
		{
			return _context.Pavimento;
		}

		[HttpGet]
		[Route("api/Pavimentos/{id}")]
		public async Task<IActionResult> Get([FromRoute] int id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var pavimento = await _context.Pavimento.SingleOrDefaultAsync(m => m.Id == id);

			if (pavimento == null)
			{
				return NotFound();
			}

			return Ok(pavimento);
		}

		[HttpPut]
		[Route("api/Pavimentos/{id}")]
		public async Task<IActionResult> Put([FromRoute] int id, [FromBody] Pavimento pavimento)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (id != pavimento.Id)
			{
				return BadRequest();
			}

			_context.Entry(pavimento).State = EntityState.Modified;

			try
			{
				await _context.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!PavimentoExists(id))
				{
					return NotFound();
				}
				else
				{
					throw;
				}
			}

			return NoContent();
		}

		[HttpPost]
		[Route("api/Pavimentos")]
		public async Task<IActionResult> Post([FromBody] Pavimento pavimento)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			_context.Pavimento.Add(pavimento);
			await _context.SaveChangesAsync();

			return CreatedAtAction("Get", new { id = pavimento.Id }, pavimento);
		}

		[HttpDelete]
		[Route("api/Pavimentos/{id}")]
		public async Task<IActionResult> Delete([FromRoute] int id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var pavimento = await _context.Pavimento.SingleOrDefaultAsync(m => m.Id == id);
			if (pavimento == null)
			{
				return NotFound();
			}

			_context.Pavimento.Remove(pavimento);
			await _context.SaveChangesAsync();

			return Ok(pavimento);
		}
		#endregion

		private bool PavimentoExists(int id)
		{
			return _context.Pavimento.Any(e => e.Id == id);
		}
	}
}

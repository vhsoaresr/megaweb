﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MegaWeb.Data;
using MegaWeb.Models;

namespace MegaWeb.Controllers
{
	public class BlocosController : Controller
	{
		private readonly FieldDBContext _context;

		public BlocosController(FieldDBContext context)
		{
			_context = context;
		}

		#region MVC
		[Route("Blocos")]
		public async Task<IActionResult> Index()
		{
			return View(await _context.Bloco.ToListAsync());
		}

		[Route("Blocos/Create")]
		public IActionResult Create()
		{
			return View();
		}

		[HttpPost]
		[Route("Blocos/Create")]
		public async Task<IActionResult> Create([Bind("Id,Descricao")] Bloco bloco)
		{
			if (ModelState.IsValid)
			{
				_context.Add(bloco);
				await _context.SaveChangesAsync();
				return RedirectToAction(nameof(Index));
			}
			return View(bloco);
		}

		[Route("Blocos/Edit/{id}")]
		public async Task<IActionResult> Edit(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var blocos = await _context.Bloco.SingleOrDefaultAsync(m => m.Id == id);
			if (blocos == null)
			{
				return NotFound();
			}
			return View(blocos);
		}
	
		[HttpPost]
		[Route("Blocos/Edit/{id}")]
		public async Task<IActionResult> Edit(int id, [Bind("Id,Descricao")] Bloco bloco)
		{
			if (id != bloco.Id)
			{
				return NotFound();
			}

			if (ModelState.IsValid)
			{
				try
				{
					_context.Update(bloco);
					await _context.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!BlocoExists(bloco.Id))
					{
						return NotFound();
					}
					else
					{
						throw;
					}
				}
				return RedirectToAction(nameof(Index));
			}
			return View(bloco);
		}

		[Route("Blocos/Delete/{id}")]
		public async Task<IActionResult> Delete(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var blocos = await _context.Bloco
				.SingleOrDefaultAsync(m => m.Id == id);
			if (blocos == null)
			{
				return NotFound();
			}

			return View(blocos);
		}

		[HttpPost, ActionName("Delete")]
		[Route("Blocos/Delete/{id}")]
		public async Task<IActionResult> DeleteConfirmed(int id)
		{
			var blocos = await _context.Bloco.SingleOrDefaultAsync(m => m.Id == id);
			_context.Bloco.Remove(blocos);
			await _context.SaveChangesAsync();
			return RedirectToAction(nameof(Index));
		}
		#endregion

		#region API
		[HttpGet]
		[Route("api/Blocos")]
		public IEnumerable<Bloco> Get()
		{
			return _context.Bloco;
		}

		[HttpGet]
		[Route("api/Blocos/{id}")]
		public async Task<IActionResult> Get([FromRoute] int id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var bloco = await _context.Bloco.SingleOrDefaultAsync(m => m.Id == id);

			if (bloco == null)
			{
				return NotFound();
			}

			return Ok(bloco);
		}


		[HttpPut]
		[Route("api/Blocos/{id}")]
		public async Task<IActionResult> Put([FromRoute] int id, [FromBody] Bloco bloco)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (id != bloco.Id)
			{
				return BadRequest();
			}

			_context.Entry(bloco).State = EntityState.Modified;

			try
			{
				await _context.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!BlocoExists(id))
				{
					return NotFound();
				}
				else
				{
					throw;
				}
			}

			return NoContent();
		}

		[HttpPost]
		[Route("api/Blocos")]
		public async Task<IActionResult> Post([FromBody] Bloco bloco)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			_context.Bloco.Add(bloco);
			await _context.SaveChangesAsync();

			return CreatedAtAction("Get", new { id = bloco.Id }, bloco);
		}

		[HttpDelete]
		[Route("api/Blocos/{id}")]
		public async Task<IActionResult> Delete([FromRoute] int id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var bloco = await _context.Bloco.SingleOrDefaultAsync(m => m.Id == id);
			if (bloco == null)
			{
				return NotFound();
			}

			_context.Bloco.Remove(bloco);
			await _context.SaveChangesAsync();

			return Ok(bloco);
		}
		#endregion

		private bool BlocoExists(int id)
		{
			return _context.Bloco.Any(e => e.Id == id);
		}
	}
}

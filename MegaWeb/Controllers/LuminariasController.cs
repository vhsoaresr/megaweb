﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MegaWeb.Data;
using MegaWeb.Models;
using System.Collections.Generic;

namespace MegaWeb.Controllers
{
	public class LuminariasController : Controller
	{
		private readonly FieldDBContext _context;

		public LuminariasController(FieldDBContext context)
		{
			_context = context;
		}

		#region MVC
		[Route("Luminarias")]
		public async Task<IActionResult> Index()
		{
			return View(await _context.Luminaria.ToListAsync());
		}
		
		[Route("Luminarias/Create")]
		public IActionResult Create()
		{
			return View();
		}
		
		[Route("Luminarias/Create")]
		[HttpPost]
		public async Task<IActionResult> Create([Bind("Id,Descricao")] Luminaria luminaria)
		{
			if (ModelState.IsValid)
			{
				_context.Add(luminaria);
				await _context.SaveChangesAsync();
				return RedirectToAction(nameof(Index));
			}
			return View(luminaria);
		}
		
		[Route("Luminarias/Edit/{id}")]
		public async Task<IActionResult> Edit(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var luminarias = await _context.Luminaria.SingleOrDefaultAsync(m => m.Id == id);
			if (luminarias == null)
			{
				return NotFound();
			}
			return View(luminarias);
		}
		
		[HttpPost]
		[Route("Luminarias/Edit/{id}")]
		public async Task<IActionResult> Edit(int id, [Bind("Id,Descricao")] Luminaria luminaria)
		{
			if (id != luminaria.Id)
			{
				return NotFound();
			}

			if (ModelState.IsValid)
			{
				try
				{
					_context.Update(luminaria);
					await _context.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!LuminariaExists(luminaria.Id))
					{
						return NotFound();
					}
					else
					{
						throw;
					}
				}
				return RedirectToAction(nameof(Index));
			}
			return View(luminaria);
		}
		
		[Route("Luminarias/Delete/{id}")]
		public async Task<IActionResult> Delete(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var luminarias = await _context.Luminaria
				.SingleOrDefaultAsync(m => m.Id == id);
			if (luminarias == null)
			{
				return NotFound();
			}

			return View(luminarias);
		}
		
		[HttpPost, ActionName("Delete")]
		[Route("Luminarias/Delete/{id}")]
		public async Task<IActionResult> DeleteConfirmed(int id)
		{
			var luminarias = await _context.Luminaria.SingleOrDefaultAsync(m => m.Id == id);
			_context.Luminaria.Remove(luminarias);
			await _context.SaveChangesAsync();
			return RedirectToAction(nameof(Index));
		}
		#endregion

		#region API
		[HttpGet]
		[Route("api/Luminarias")]
		public IEnumerable<Luminaria> Get()
		{
			return _context.Luminaria;
		}
		
		[HttpGet]
		[Route("api/Luminarias/{id}")]
		public async Task<IActionResult> Get([FromRoute] int id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var luminaria = await _context.Luminaria.SingleOrDefaultAsync(m => m.Id == id);

			if (luminaria == null)
			{
				return NotFound();
			}

			return Ok(luminaria);
		}
		
		[HttpPut]
		[Route("api/Luminarias/{id}")]
		public async Task<IActionResult> Put([FromRoute] int id, [FromBody] Luminaria luminaria)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (id != luminaria.Id)
			{
				return BadRequest();
			}

			_context.Entry(luminaria).State = EntityState.Modified;

			try
			{
				await _context.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!LuminariaExists(id))
				{
					return NotFound();
				}
				else
				{
					throw;
				}
			}

			return NoContent();
		}
		
		[HttpPost]
		[Route("api/Luminarias")]
		public async Task<IActionResult> Post([FromBody] Luminaria luminaria)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			_context.Luminaria.Add(luminaria);
			await _context.SaveChangesAsync();

			return CreatedAtAction("Get", new { id = luminaria.Id }, luminaria);
		}

		[HttpDelete]
		[Route("api/Luminarias/{id}")]
		public async Task<IActionResult> Delete([FromRoute] int id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var luminaria = await _context.Luminaria.SingleOrDefaultAsync(m => m.Id == id);
			if (luminaria == null)
			{
				return NotFound();
			}

			_context.Luminaria.Remove(luminaria);
			await _context.SaveChangesAsync();

			return Ok(luminaria);
		}
		#endregion

		private bool LuminariaExists(int id)
		{
			return _context.Luminaria.Any(e => e.Id == id);
		}
	}
}

﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MegaWeb.Data;
using MegaWeb.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace MegaWeb.Controllers
{
	public class LevantamentosController : Controller
	{
		private readonly FieldDBContext _context;

		public LevantamentosController(FieldDBContext context)
		{
			_context = context;
		}

		#region MVC
		[Route("Levantamentos")]
		public async Task<IActionResult> Index()
		{
			var fieldDBContext = _context.Levantamento.Include(l => l.IdBlocoNavigation).Include(l => l.IdEstadoNavigation).Include(l => l.IdLuminariaNavigation).Include(l => l.IdPavimentoNavigation);
			return View(await fieldDBContext.ToListAsync());
		}

		[Route("Levantamentos/Create")]
		public IActionResult Create()
		{
			ViewData["IdBloco"] = new SelectList(_context.Bloco, "Id", "Descricao");
			ViewData["IdEstado"] = new SelectList(_context.Estado, "Id", "Descricao");
			ViewData["IdLuminaria"] = new SelectList(_context.Luminaria, "Id", "Descricao");
			ViewData["IdPavimento"] = new SelectList(_context.Pavimento, "Id", "Descricao");
			return View();
		}

		[HttpPost]
		[Route("Levantamentos/Create")]
		public async Task<IActionResult> Create([Bind("Id,Descricao,IdPavimento,IdBloco,IdLuminaria,IdEstado,Imagem,IsConcluido")] Levantamento levantamento)
		{
			if (ModelState.IsValid)
			{
				_context.Add(levantamento);
				await _context.SaveChangesAsync();
				return RedirectToAction(nameof(Index));
			}
			ViewData["IdBloco"] = new SelectList(_context.Bloco, "Id", "Descricao", levantamento.IdBloco);
			ViewData["IdEstado"] = new SelectList(_context.Estado, "Id", "Descricao", levantamento.IdEstado);
			ViewData["IdLuminaria"] = new SelectList(_context.Luminaria, "Id", "Descricao", levantamento.IdLuminaria);
			ViewData["IdPavimento"] = new SelectList(_context.Pavimento, "Id", "Descricao", levantamento.IdPavimento);
			return View(levantamento);
		}
		
		[Route("Levantamentos/Edit/{id}")]
		public async Task<IActionResult> Edit(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var levantamento = await _context.Levantamento.SingleOrDefaultAsync(m => m.Id == id);
			if (levantamento == null)
			{
				return NotFound();
			}
			ViewData["IdBloco"] = new SelectList(_context.Bloco, "Id", "Descricao", levantamento.IdBloco);
			ViewData["IdEstado"] = new SelectList(_context.Estado, "Id", "Descricao", levantamento.IdEstado);
			ViewData["IdLuminaria"] = new SelectList(_context.Luminaria, "Id", "Descricao", levantamento.IdLuminaria);
			ViewData["IdPavimento"] = new SelectList(_context.Pavimento, "Id", "Descricao", levantamento.IdPavimento);
			return View(levantamento);
		}
		
		[HttpPost]
		[Route("Levantamentos/Edit/{id}")]
		public async Task<IActionResult> Edit(int id, [Bind("Id,Descricao,IdPavimento,IdBloco,IdLuminaria,IdEstado,Imagem,IsConcluido")] Levantamento levantamento)
		{
			if (id != levantamento.Id)
			{
				return NotFound();
			}

			if (ModelState.IsValid)
			{
				try
				{
					_context.Update(levantamento);
					await _context.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!LevantamentoExists(levantamento.Id))
					{
						return NotFound();
					}
					else
					{
						throw;
					}
				}
				return RedirectToAction(nameof(Index));
			}
			ViewData["IdBloco"] = new SelectList(_context.Bloco, "Id", "Descricao", levantamento.IdBloco);
			ViewData["IdEstado"] = new SelectList(_context.Estado, "Id", "Descricao", levantamento.IdEstado);
			ViewData["IdLuminaria"] = new SelectList(_context.Luminaria, "Id", "Descricao", levantamento.IdLuminaria);
			ViewData["IdPavimento"] = new SelectList(_context.Pavimento, "Id", "Descricao", levantamento.IdPavimento);
			return View(levantamento);
		}
		
		[Route("Levantamentos/Delete/{id}")]
		public async Task<IActionResult> Delete(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var levantamento = await _context.Levantamento
				.Include(l => l.IdBlocoNavigation)
				.Include(l => l.IdEstadoNavigation)
				.Include(l => l.IdLuminariaNavigation)
				.Include(l => l.IdPavimentoNavigation)
				.SingleOrDefaultAsync(m => m.Id == id);
			if (levantamento == null)
			{
				return NotFound();
			}

			return View(levantamento);
		}
		
		[HttpPost, ActionName("Delete")]
		[Route("Levantamentos/Delete/{id}")]
		public async Task<IActionResult> DeleteConfirmed(int id)
		{
			var levantamento = await _context.Levantamento.SingleOrDefaultAsync(m => m.Id == id);
			_context.Levantamento.Remove(levantamento);
			await _context.SaveChangesAsync();
			return RedirectToAction(nameof(Index));
		}
		#endregion

		#region API
		[HttpGet]
		[Route("api/Levantamentos")]
		public IEnumerable<Levantamento> Get()
		{
			return _context.Levantamento.Where(o=> !o.IsConcluido);
		}

		[HttpGet]
		[Route("api/Levantamentos/{id}")]
		public async Task<IActionResult> Get([FromRoute] int id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var levantamento = await _context.Levantamento.SingleOrDefaultAsync(m => m.Id == id);

			if (levantamento == null)
			{
				return NotFound();
			}

			return Ok(levantamento);
		}

		[HttpPut]
		[Route("api/Levantamentos/{id}")]
		public async Task<IActionResult> Put([FromRoute] int id, [FromBody] Levantamento levantamento)
		{
			if (!ModelState.IsValid)
				return BadRequest(ModelState);

			if (id != levantamento.Id)
				return BadRequest();

			_context.Entry(levantamento).State = EntityState.Modified;

			try
			{
				await _context.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!LevantamentoExists(id))
					return NotFound();
				else
					throw;
			}

			return NoContent();
		}

		[HttpPost]
		[Route("api/Levantamentos")]
		public async Task<IActionResult> Post([FromBody] Levantamento levantamento)
		{
			if (!ModelState.IsValid)
				return BadRequest(ModelState);

			_context.Levantamento.Add(levantamento);
			try
			{
				await _context.SaveChangesAsync();
			}
			catch (DbUpdateException)
			{
				if (LevantamentoExists(levantamento.Id))
					return new StatusCodeResult(StatusCodes.Status409Conflict);
				else
					throw;
			}

			return CreatedAtAction("GetLevantamento", new { id = levantamento.Id }, levantamento);
		}

		[HttpDelete]
		[Route("api/Levantamentos/{id}")]
		public async Task<IActionResult> Delete([FromRoute] int id)
		{
			if (!ModelState.IsValid)
				return BadRequest(ModelState);

			var levantamento = await _context.Levantamento.SingleOrDefaultAsync(m => m.Id == id);
			if (levantamento == null)
				return NotFound();

			_context.Levantamento.Remove(levantamento);
			await _context.SaveChangesAsync();

			return Ok(levantamento);
		}
		#endregion

		private bool LevantamentoExists(int id)
		{
			return _context.Levantamento.Any(e => e.Id == id);
		}
	}
}

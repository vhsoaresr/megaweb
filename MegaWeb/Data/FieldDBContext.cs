﻿using System;
using System.IO;
using MegaWeb.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

namespace MegaWeb.Data
{
	public partial class FieldDBContext : DbContext
	{
		public FieldDBContext(DbContextOptions<FieldDBContext> options) : base(options) { }

		public virtual DbSet<Bloco> Bloco { get; set; }
		public virtual DbSet<Estado> Estado { get; set; }
		public virtual DbSet<Levantamento> Levantamento { get; set; }
		public virtual DbSet<Luminaria> Luminaria { get; set; }
		public virtual DbSet<Pavimento> Pavimento { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			if (!optionsBuilder.IsConfigured)
			{
				var configuration = new ConfigurationBuilder()
					.SetBasePath(Directory.GetCurrentDirectory())
					.AddJsonFile("appsettings.json")
					.Build();

				optionsBuilder.UseSqlServer(configuration.GetConnectionString("FieldDBDatabase"));
			}
		}
		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Bloco>(entity =>
			{
				entity.Property(e => e.Descricao)
					.HasMaxLength(100)
					.IsUnicode(false);
			});

			modelBuilder.Entity<Estado>(entity =>
			{
				entity.Property(e => e.Descricao)
					.HasMaxLength(100)
					.IsUnicode(false);
			});

			modelBuilder.Entity<Levantamento>(entity =>
			{
				entity.Property(e => e.Descricao)
					.HasMaxLength(100)
					.IsUnicode(false);

				entity.Property(e => e.Imagem).HasColumnType("image");

				entity.HasOne(d => d.IdBlocoNavigation)
					.WithMany(p => p.Levantamento)
					.HasForeignKey(d => d.IdBloco)
					.HasConstraintName("FK_Levantamento_Bloco");

				entity.HasOne(d => d.IdEstadoNavigation)
					.WithMany(p => p.Levantamento)
					.HasForeignKey(d => d.IdEstado)
					.HasConstraintName("FK_Levantamento_Estado");

				entity.HasOne(d => d.IdLuminariaNavigation)
					.WithMany(p => p.Levantamento)
					.HasForeignKey(d => d.IdLuminaria)
					.HasConstraintName("FK_Levantamento_Luminaria");

				entity.HasOne(d => d.IdPavimentoNavigation)
					.WithMany(p => p.Levantamento)
					.HasForeignKey(d => d.IdPavimento)
					.HasConstraintName("FK_Levantamento_Pavimento");
			});

			modelBuilder.Entity<Luminaria>(entity =>
			{
				entity.Property(e => e.Descricao)
					.HasMaxLength(100)
					.IsUnicode(false);
			});

			modelBuilder.Entity<Pavimento>(entity =>
			{
				entity.Property(e => e.Descricao)
					.HasMaxLength(100)
					.IsUnicode(false);
			});
		}
	}
}
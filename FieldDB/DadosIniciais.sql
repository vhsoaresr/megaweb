﻿INSERT INTO [Bloco]([Descricao]) VALUES ('Bloco A')
INSERT INTO [Bloco]([Descricao]) VALUES ('Bloco B')
INSERT INTO [Bloco]([Descricao]) VALUES ('Bloco C')
INSERT INTO [Bloco]([Descricao]) VALUES ('Bloco D')

INSERT INTO [Pavimento]([Descricao]) VALUES ('1º Pavimento')
INSERT INTO [Pavimento]([Descricao]) VALUES ('2º Pavimento')
INSERT INTO [Pavimento]([Descricao]) VALUES ('3º Pavimento')
INSERT INTO [Pavimento]([Descricao]) VALUES ('4º Pavimento')

INSERT INTO [Estado]([Descricao]) VALUES ('Funcionando')
INSERT INTO [Estado]([Descricao]) VALUES ('Queimada')

INSERT INTO [Luminaria]([Descricao]) VALUES ('Luminária 1')
INSERT INTO [Luminaria]([Descricao]) VALUES ('Luminária 2')
INSERT INTO [Luminaria]([Descricao]) VALUES ('Luminária 3')
INSERT INTO [Luminaria]([Descricao]) VALUES ('Luminária 4')

INSERT INTO [Levantamento]([Descricao], [idPavimento], [idBloco], [idLuminaria], [idEstado], [imagem],[isConcluido]) VALUES ('Levantamento 1', 1, 1, 1, 1, NULL,0)

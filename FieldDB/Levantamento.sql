﻿CREATE TABLE [dbo].[Levantamento]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Descricao] VARCHAR(100) NULL, 
    [IdPavimento] INT NULL, 
    [IdBloco] INT NULL, 
    [IdLuminaria] INT NULL, 
    [IdEstado] INT NULL, 
    [Imagem] IMAGE NULL, 
    [IsConcluido] BIT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_Levantamento_Pavimento] FOREIGN KEY ([IdPavimento]) REFERENCES [Pavimento]([id]),
    CONSTRAINT [FK_Levantamento_Bloco] FOREIGN KEY ([IdBloco]) REFERENCES [Bloco]([id]),
    CONSTRAINT [FK_Levantamento_Luminaria] FOREIGN KEY ([IdLuminaria]) REFERENCES [Luminaria]([id]),
    CONSTRAINT [FK_Levantamento_Estado] FOREIGN KEY ([IdEstado]) REFERENCES [Estado]([id]) 
)
